 (function() {

    'use strict';

    Deeboo.Routers = Deeboo.Routers || {};

    Deeboo.Routers.Router = Backbone.Router.extend({
        routes: {
            '': 'index'
        },

        index: Deeboo.Handlers.indexHandler
    });

}());
