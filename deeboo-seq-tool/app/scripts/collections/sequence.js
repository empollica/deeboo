/*global define*/

(function(){
    'use strict';

    Deeboo.Collections = Deeboo.Collections || {};

    Deeboo.Collections.Sequence = Backbone.Collection.extend({

        model: Deeboo.Models.Tick,

        getStringRepresentation: function () {

            var stringRepresentation = '';

            _.each(this.models, function(tick){
                stringRepresentation += '{' + tick.get('r') + ',' + tick.get('g') + ',' + tick.get('b')  + ',' + tick.get('leftLed')  + ',' + tick.get('rightLed') + '},';
            }, this);
            return stringRepresentation.substr(0, stringRepresentation.length - 1);
        }
    });
}());



