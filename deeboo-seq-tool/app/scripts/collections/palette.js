/*global define*/

(function() {
    'use strict';

    Deeboo.Collections = Deeboo.Collections || {};

    Deeboo.Collections.Palette = Backbone.Collection.extend({
        model: Deeboo.Models.Color
    });
}());


