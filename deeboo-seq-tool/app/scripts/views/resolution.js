(function(){

    'use strict';

    Deeboo.Views = Deeboo.Views || {};

    Deeboo.Views.Resolution = Backbone.View.extend({

        tagName: 'span',

        el: '#resolution',

        className: 'deeboo-resolution',

        events: {
            'click':  'onClick'
        },

        initialize: function () {
        },

        render: function () {
            this.$el.html(Deeboo.Resolution/Deeboo.MaxResolution);
            return this;
        },

        onClick: function () {

            Deeboo.Resolution++;

            if(Deeboo.Resolution == Deeboo.MaxResolution + 1) {
                Deeboo.Resolution = 1;
            }

            this.render();
        }
    });

}());
