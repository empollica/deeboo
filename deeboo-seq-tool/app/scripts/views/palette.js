(function(){

    'use strict';

    Deeboo.Views = Deeboo.Views || {};

    Deeboo.Views.Palette = Backbone.View.extend({

        tagName: 'ul',

        el: '#palette',

        className: 'deeboo-palette',

        events: {},

        initialize: function (options) {

            this.collection = options.collection;
            _.bindAll(this, 'render');

            this.listenTo(this.collection, 'render', this.render);
            this.listenTo(this.collection, 'add', this.render);
            this.listenTo(this.collection, 'remove', this.render);
        },

        render: function () {
            this.$el.empty();
            _.each(this.collection.models, function(color){

                var colorView = new Deeboo.Views.Color({
                    model: color
                });

                this.$el.append(colorView.render().$el);

            }, this);

            return this;
        }
    });

}());
