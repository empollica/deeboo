(function(){

    'use strict';

    Deeboo.Views = Deeboo.Views || {};

    Deeboo.Views.Sequence = Backbone.View.extend({

        tagName: 'ul',

        el: '#sequence',

        className: 'deeboo-sequence',

        events: {},

        initialize: function (options) {

            this.collection = options.collection;
            _.bindAll(this, 'render');

            this.listenTo(this.collection, 'render', this.render);
            this.listenTo(this.collection, 'add', this.render);
            this.listenTo(this.collection, 'remove', this.render);
        },

        render: function () {
            this.$el.empty();
            _.each(this.collection.models, function(tick, i){

                var tickView = new Deeboo.Views.Tick({
                    model: tick
                });

                var $viewEl = tickView.render().$el;
                if (i % 4 === 0) {
                    $viewEl.addClass('beat');
                }
                if (i % 32 === 0) {
                    $viewEl.addClass('phrase');
                }
                this.$el.append($viewEl);

            }, this);

            return this;
        }
    });

}());
