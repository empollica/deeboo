(function(){

    'use strict';

    Deeboo.Views = Deeboo.Views || {};

    Deeboo.Views.Color = Backbone.View.extend({

        tagName: 'li',

        className: 'deeboo-palette-color',

        events: {
            'click': 'onClick'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        onClick: function () {
            this.model.toggleSelected();
        },

        render: function () {

            this.$el.toggleClass('selected', this.model.get('selected'));

            this.$el.css({'background-color': this.model.getCssColor()});
            return this;
        }
    });

}());
