/*global define*/

(function(){

    'use strict';

    Deeboo.Views = Deeboo.Views || {};

    Deeboo.Views.Tick = Backbone.View.extend({

        tagName: 'li',

        template: _.template('<div class="leftLed"></div><div class="strip"></div><div class="rightLed"></div>'),
        className: 'deeboo-sequence-tick',

        events: {
            'click .leftLed':  'onLeftLedClick',
            'click .rightLed':  'onRightLedClick',
            'click .strip':  'onStripClick'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        render: function () {

            var $template = $(this.template()),
                $leftLed = $($template[0]),
                $strip = $($template[1]),
                $rightLed = $($template[2]);

            $strip.css({'background-color': (this.model.getStripCssColor() === 'rgb(0,0,0)' ? '#333' : this.model.getStripCssColor())});
            $leftLed.css({'background-color': this.model.get('leftLed') === 1 ? '#FFF' : '#333'});
            $rightLed.css({'background-color': this.model.get('rightLed') === 1 ? '#FFF' : '#333'});

            this.$el.empty().append($leftLed).append($strip).append($rightLed);

            return this;
        },

        onLeftLedClick: function () {
            this.model.toggleLed('leftLed');
            Deeboo.Events.trigger('tick:ledClick', {order: this.model.order, led: 'leftLed', value: this.model.get('leftLed')});
        },

        onRightLedClick: function () {
            this.model.toggleLed('rightLed');
            Deeboo.Events.trigger('tick:ledClick', {order: this.model.order, led: 'rightLed', value: this.model.get('rightLed')});
        },

        onStripClick: function () {
            this.model.updateRGB();
            Deeboo.Events.trigger('tick:stripClick', this.model.order);
        }
    });

}());
