(function(){
    'use strict';

    Deeboo.Handlers = Deeboo.Handlers || {};

    Deeboo.Handlers.indexHandler = function() {

            var palette = {},
                sequence = {},
                resolution = {};

            Deeboo.Events.on('color:colorSelected', function(selectedColorModel){
                _.each(palette.collection.models, function(color){
                    color.set('selected', false);
                });

                _.each(sequence.collection.models, function(tick){
                    tick.selectedColor = selectedColorModel.toJSON();
                });
            });

            Deeboo.Events.on('tick:ledClick', function(tick) {
                for (var i = 1; i < Deeboo.Resolution; i++) {
                    if (_.isUndefined(sequence.collection.models[tick.order + i]))
                        return;
                    sequence.collection.models[tick.order + i].set(tick.led, tick.value);
                };
            });

            Deeboo.Events.on('tick:stripClick', function(tickOrder) {
                for (var i = 1; i < Deeboo.Resolution; i++) {
                    if (_.isUndefined(sequence.collection.models[tickOrder + i]))
                        return;
                    sequence.collection.models[tickOrder + i].updateRGB();
                };
            });

            Deeboo.Events.on('tick:updated', function() {
                $('#output').html(sequence.collection.getStringRepresentation());
            });

            palette.collection = new Deeboo.Collections.Palette([
                {r: 0, g: 0, b: 0, selected: true},
                {r: 255, g:0, b: 0},
                {r: 0, g: 255, b: 0},
                {r: 0, g: 0, b: 255},
                {r: 255, g: 255, b: 0},
                {r: 255, g: 0, b: 255},
                {r: 0, g: 255, b: 255},
                {r: 255, g: 255, b: 255}
            ]);

            palette.view = new Deeboo.Views.Palette({collection: palette.collection});

            palette.view.render();

            sequence.collection = new Deeboo.Collections.Sequence();
            for (var i=0; i<128; i++) {
                sequence.collection.add({order: i});
            }

            sequence.view = new Deeboo.Views.Sequence({collection: sequence.collection});
            sequence.view.render();

            resolution.view = new Deeboo.Views.Resolution();
            resolution.view.render();
        };
}());
