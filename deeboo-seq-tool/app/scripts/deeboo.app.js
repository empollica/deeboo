(function(){
    window.Deeboo = {
        Models: {},
        Collections: {},
        Views: {},
        Routers: {},
        Handlers: {},
        Events: _.extend({}, Backbone.Events),
        Resolution: 4,
        MaxResolution: 4
    };


    $(document).ready(function() {

        var router = new Deeboo.Routers.Router();
        Backbone.history.start();

    });

}());
