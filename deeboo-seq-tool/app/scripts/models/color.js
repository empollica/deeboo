(function() {

    'use strict';

    Deeboo.Models = Deeboo.Models || {};

    Deeboo.Models.Color = Backbone.Model.extend({
        url: '',

        initialize: function() {
        },

        defaults: {
            selected: false
        },

        getCssColor: function() {
            return 'rgb('+this.get('r')+','+this.get('g')+','+this.get('b')+')';
        },

        toggleSelected: function() {
            Deeboo.Events.trigger('color:colorSelected', this);
            this.set('selected', this.get('selected') ? false : true);
        }
    });
}());
