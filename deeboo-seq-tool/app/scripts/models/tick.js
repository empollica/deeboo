/*global define*/

(function(){

    'use strict';

    Deeboo.Models = Deeboo.Models || {};

    Deeboo.Models.Tick = Backbone.Model.extend({

        selectedColor: {
            r: 0,
            g: 0,
            b: 0
        },

        defaults: {
            r: 0,
            g: 0,
            b: 0,
            leftLed: 0,
            rightLed: 0
        },

        initialize: function(options) {
            this.order = options.order;
            this.listenTo(this, 'change', function(){
                Deeboo.Events.trigger('tick:updated');
            });
        },

        getStripCssColor: function() {
            return 'rgb('+this.get('r')+','+this.get('g')+','+this.get('b')+')';
        },

        toggleLed: function(led) {
            var oldVar = this.get(led);
            this.set(led, (oldVar === 0 ? 1 : 0));
        },

        updateRGB: function() {
            this.set({r: this.selectedColor.r, g: this.selectedColor.g, b: this.selectedColor.b})
        }

    });
}());
