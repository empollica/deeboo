# Deeboo #

Arduino based DIY MIDI synced lights system for bedroom djs. The system is composed by an Arduino board plus DIY pcb board for controlling an RGB led strip and two additional white led bulbs.

The project includes a Yeoman-Backbone generated webtool to allow users to create light sequences to be flashed into Arduino's program.

### Status ###
Prototype working. Final design and PCB construction under development

### Technologies ###

* Arduino - C
* Backbone.js
* Yeoman - Grunt - Bower
* SASS

### How it works ###

* Create light sequences by using Deeboo Sequence Webtool.
* Connect board USB to computer
* Copy generated sequences to Arduino software and flash Arduino board.
* Configure [Hairless Midi to Serial converter](http://projectgus.github.io/hairless-midiserial/) to connect Deeboo board to your MIDI software e.g. Traktor DJ software.
* Start sending MIDI clock signal from your DJ software to Deeboo board and light sequences will be "played" in sync with your music.
* Enjoy.

### Deeboo Sequence Webtool ###

![deeboo_seq_tool.png](https://bitbucket.org/repo/rn8a5x/images/76468382-deeboo_seq_tool.png)

### See deeboo in action ###
[![deeboo video.png](https://bitbucket.org/repo/rn8a5x/images/2948840270-deeboo%20video.png)](https://www.youtube.com/watch?v=yWp43achVTo)